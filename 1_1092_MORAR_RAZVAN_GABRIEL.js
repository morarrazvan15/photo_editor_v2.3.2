const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const uploadfisier = document.getElementById("uploadFisier");
const resetButon = document.getElementById("resetareButon");
const resolutieButon=document.getElementById("resolution-btn");
const AddPackmanButton=document.getElementById("PackManFigure");
const butonDownload = document.getElementById("ButonDownload");
let imagine = new Image();
let numeFisier = "";
let heightX;
let heightY;

uploadfisier.addEventListener("change", () => {
  const citire = new FileReader();
  const file = document.getElementById("uploadFisier").files[0];
  if (file) {
    citire.readAsDataURL(file);
    numeFisier = file.name;
  }


  citire.addEventListener(
    "load",
    () => {
      imagine = new Image();
      imagine.src = citire.result;
      imagine.onload = function() {
        
        canvas.height = imagine.height;
        heightX=imagine.height;
        console.log("LOADED FUNCTION STARTS");
        canvas.width = imagine.width;
        heightY=imagine.width;
        ctx.drawImage(imagine, 0, 0, heightY, heightX);
        canvas.removeAttribute("data-caman-id");
        
      };
    },
    false
  );
});

let paint=false;
        function startPosition()
        {
          paint=true;
        }
        function finishPosition()
        {
          paint=false;
        }

        function draw(e){
          if(!paint) return;
          ctx.lineWidth=10;
          ctx.lineCap=null;
          ctx.lineTo(e.clientX,e.clientY);
          //ctx.lineCap(canvas.height,canvas.width);
          ctx.stroke();
        }
canvas.addEventListener("mousedown",startPosition);
canvas.addEventListener("mouseup",finishPosition);
canvas.addEventListener("mousemove",draw);

AddPackmanButton.addEventListener("click",e=>{
 
  ctx.beginPath();
  ctx.arc(600, 300, 50, 0.25 * Math.PI, 1.25 * Math.PI, false);
  ctx.fillStyle = "yellow";
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.arc(600, 300, 50, 0.75 * Math.PI, 1.75 * Math.PI, false);
  ctx.fill();
  ctx.closePath();
  ctx.beginPath();
  ctx.arc(600,280, 10, 0, 2 * Math.PI, false);
  ctx.fillStyle = "green";
  ctx.fill();
  ctx.closePath(); 
  
  ctx.beginPath();
  ctx.arc(600,320, 10, 0, 2 * Math.PI, false);
  ctx.fillStyle = "green";
  ctx.fill();
  ctx.closePath();
});

resetButon.addEventListener("click", e => {
  Caman("#canvas", imagine, function() {
    console.log("RESET BUTTON CLICKED");
    this.revert();
    ctx.clearRect(0,0,canvas.width,canvas.height);
    //canvas.height=heightX;
    //canvas.width=heightY;
    ctx.drawImage(imagine, 0, 0, canvas.width, canvas.height);
        canvas.removeAttribute("data-caman-id");
  
  });
});

butonDownload.addEventListener("click", () => {

  const fileExtension = numeFisier.slice(-4);
  let NumeNouFisier;
  
  if(fileExtension===".png")
  {
    NumeNouFisier = "MORARRAZVANGABRIEL"+numeFisier.substring(0, numeFisier.length - 4) + ".png";
  }
  else if(fileExtension===".jpg")
  {
    NumeNouFisier = "MORARRAZVANGABRIEL"+numeFisier.substring(0, numeFisier.length - 4) + ".jpg";
  }
  else if (fileExtension===".GIF")
  {
    NumeNouFisier = "MORARRAZVANGABRIEL"+numeFisier.substring(0, numeFisier.length - 4) + ".GIF";
  }


  download(canvas, NumeNouFisier);
});

function download(canvas, numeFisier) {

  console.log("DOWNLOAD BUTTON CLICKED");
  let variab;
  const link = document.createElement("a");
  link.download = numeFisier;
  link.href = canvas.toDataURL("image/jpeg", 0.8);
  variab = new MouseEvent("click");
  link.dispatchEvent(variab);
};

document.addEventListener("click", e => {

const check_var=1;

  if (e.target.classList.contains("filter-btn") &&(check_var==1)) {

    console.log("EFFECT APPLIED");
    if (e.target.classList.contains("brightness-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.brightness(5);
        holder.render();
 
      });
    } 
    if (e.target.classList.contains("brightness-remove")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.brightness(-5);
        holder.render();
      
      });
    }
    if (e.target.classList.contains("contrast-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.contrast(7);
        holder.render();

      });
    }
    if (e.target.classList.contains("contrast-remove")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.contrast(-7);
        holder.render();

      });
    }
    if (e.target.classList.contains("saturation-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.saturation(7);
        holder.render();
     
      });
    }
    if (e.target.classList.contains("saturation-remove")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.saturation(-7);
        holder.render();
    
      });
    }
    if (e.target.classList.contains("vibrance-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.vibrance(7);
        holder.render();

      });
    }
    if (e.target.classList.contains("vibrance-remove")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.vibrance(-7);
        holder.render();
  
      });
    }
    if (e.target.classList.contains("vintage-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {console.log("EFFECT APPLIED");
      console.log("EFFECT APPLIED");
      let holder=this.vintage();
      holder.render();
      });
    }
    if (e.target.classList.contains("love-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.love();
        holder.render();

      });
    }
    if (e.target.classList.contains("clarity-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.clarity();
        holder.render();

        
      });
    }
    if (e.target.classList.contains("sincity-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.sinCity();
        holder.render();
      
      });
    }
    if (e.target.classList.contains("crossprocess-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.crossProcess();
        holder.render();
    
        
      });
    }
    if (e.target.classList.contains("pinhole-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.pinhole();
        holder.render();
       
      });
    }
    if (e.target.classList.contains("nostalgia-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.nostalgia();
        holder.render();
    
      });
    }
    if (e.target.classList.contains("hermajesty-add")&&(check_var==1)) {
      Caman("#canvas", imagine, function() {
        console.log("EFFECT APPLIED");
        let holder=this.herMajesty();
        holder.render();

      });
    }
  }
});



resolutieButon.addEventListener("click",()=>{

    imagine = new Image();
      imagine.src = citire.result;
      imagine.style.height = '100px';
      imagine.style.width = '200px';
      citire.addEventListener(
        "load",
        () => {
          imagine = new Image();
          imagine.src = citire.result;
          imagine.onload = function() {
            imagine.width=1920;
            imagine.height=1080;
            canvas.width = imagine.width;
            canvas.height = imagine.height;
            ctx.drawImage(imagine, 0, 0, imagine.width, imagine.height);
            canvas.removeAttribute("data-caman-id");
          };
        },
        false
      );
  
},false);










